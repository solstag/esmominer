#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# esmo.py -- script to extract and manipulate data from ESMO conferences
#
# Copyright (C) 2016-2016 Alexandre Hannud Abdo <abdo@member.fsf.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Standard libraries
import argparse, sys, os, json, re, time, tempfile, subprocess
from multiprocessing import Pool
from math import sqrt

# External libraries
from lxml import etree, html
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import XMLConverter
from pdfminer.layout import LAParams

# Python version specific imports
if sys.version_info >= (3,):
    from urllib.request import urlopen
else:
    from urllib2 import urlopen
    import codecs
    sys.stdout = codecs.getwriter("utf-8")(sys.stdout)

# Local imports
from EsmoDescription import EsmoDescription

# Command line options
parser = argparse.ArgumentParser(
    description='Find, download and extract ESMO abstracts and TOCs')
parser.add_argument('years', metavar='YYYY [YYYY ...]', type=str, nargs='*',
    help='Run for the given list of years')
parser.add_argument('-l', '--list', action='store_true',
    help='list available years and other conference info, then exits')
parser.add_argument('-s', '--save-dir', default='auto_esmo',
    help='save files into directory (default: auto_esmo)')
parser.add_argument('--get-tocs', action='store_true',
    help='download the conferences\' TOCs')
parser.add_argument('--get-abstracts', action='store_true',
    help='scrapes the first 17 items from each conference and saves some metadata')
parser.add_argument('--download', action='store_true',
    help = 'requires --abstracts; ' + \
           'downloads the actual files (make sure you are not behind a paywall)')
parser.add_argument('--all-of-them', action='store_true',
    help = 'requires --abstracts; ' + \
           'process all items from each conference ' + \
           '(might result in your IP being blacklisted)')
parser.add_argument('--pdf-do-ocr', action='store_true',
    help='OCR downloaded pdfs and embed the text (needs tesseract and convert)')
parser.add_argument('--pdf-to-xml', action='store_true',
    help='Convert downloaded pdfs to xml')
parser.add_argument('--xml-to-json', action='store_true',
    help='Extract abstracts from xml into json')
parser.add_argument('--html-to-json', action='store_true',
    help='Extract data from html')
parser.add_argument('--print-abstracts', action='store_true',
    help='Print abstracts! (from json)')
parser.add_argument('--changes', action='store_true',
    help='Tools to assist improving parsing parameters')
parser.add_argument('--update', action='store_true',
    help='requires --changes; Reprocesses xml_to_json, creating prints and diffs')
parser.add_argument('--accept', action='store_true',
    help='requires --changes; Makes current prints the new basis for comparison')
parser.add_argument('--view', action='store_true',
    help='requires --changes; Opens the current accepted prints')
parser.add_argument('--diff', action='store_true',
    help='requires --changes; Opens the last saved diffs')
parser.add_argument('--inspect', action='store_true',
    help='requires --view or --diff; Also opens the json from xml')
parser.add_argument('--processes', metavar='P', type=int, default=None,
    help='use P processes when parallelizing (default: os.cpu_count())')
parser.add_argument('-v', '--verbose', metavar='V', type=int, default=1,
    help='verbosity level; 0 is quiet (default: 1)')
args = parser.parse_args()

# Main
def main(argv):
    esmo_desc = EsmoDescription()
    if args.list:
        print(esmo_desc)
        return
    if not args.years:
        print('\nPlease specify a sequence of one or more years.')
        print('Run --list to see available years or --help for instructions.\n')
    for desc in esmo_desc:
        if desc['year'] not in args.years:
            continue
        c = EsmoConference(**desc,
                           save_dir=args.save_dir,
                           processes=args.processes)
        print('\nESMO Conference info:\n{}\n'.format(c))
        if args.get_tocs:
            c.save_toc()
        if args.get_abstracts:
            c.walk_search(c.save_search_items,
                          all_of_them=args.all_of_them,
                          download=args.download)
        if args.pdf_do_ocr:
                c.all_pdf_to_tess()
        if args.pdf_to_xml:
            if 'ocr' in c.flags:
                c.all_pdf_to_xml()
            elif 'img' in c.flags:
                c.all_pdf_to_xml(c.xmldir)
        if args.xml_to_json:
            c.all_xml_to_json()
        if args.print_abstracts:
            c.print_abstracts()
        if args.html_to_json:
            c.all_html_to_content()
        if args.changes:
            c.changes(args.accept, args.update, args.view, args.diff, args.inspect)

# Functions
def vprint(s, level=0, verbose=args.verbose):
    if level < verbose:
        print(s)

def convert_pdf_to_tess(pdf_path, tess_path, params):
    def clean_objects(fname):
        from scipy import ndimage, misc
        import numpy
        img = ndimage.imread(fname)
        cmin, cmax = ndimage.extrema(img)[:2]
        flabels, fnum = ndimage.label(numpy.where(img>0, 0, 1))
        for feat, obj in zip(range(1, fnum+1), ndimage.find_objects(flabels)):
            if all( (x.stop - x.start) > 50 for x in obj ) or \
               any( (x.stop - x.start) > 100 for x in obj ):
                img = numpy.where(flabels==feat, cmax, img)
        img[:,:20] = img[:,-20:] = cmax
        misc.imsave(fname, img)

    with tempfile.TemporaryDirectory(prefix='esmopy-', dir='.') as tdir:
        pages_re = re.compile('page-(\d+)\.png')
        pages_root = os.path.join(tdir, 'page-%d.png')
        tiff_path = os.path.join(tdir, 'document.tiff')
        convert_params = '-colorspace gray -threshold 50% -alpha off'.split()
        subprocess.run(['convert'] + [pdf_path] + convert_params + [pages_root])
        get_pagenum = lambda x: int(pages_re.match(x).groups()[0])
        pages_sorted = sorted(filter(pages_re.match, os.listdir(tdir)), key=get_pagenum)
        pages_paths = [os.path.join(tdir, d) for d in pages_sorted]
        for ppath in pages_paths:
            clean_objects(ppath)
        convert_params = '-units PixelsPerInch -density {}'.format(params['dpi']).split()
        subprocess.run(['convert'] + pages_paths + convert_params + [tiff_path])
        subprocess.run(['tesseract', tiff_path, tess_path, 'pdf'])

def convert_pdf_to_xml(pdf_path, xml_path, params):
    codec = 'utf-8'
    rsrcmgr = PDFResourceManager(caching=True)
    laparams = LAParams()
    if 'laparams_char_margin' in params:
        laparams.char_margin = float(params['laparams_char_margin'])
    if 'laparams_line_margin' in params:
        laparams.line_margin = float(params['laparams_line_margin'])
    if 'laparams_word_margin' in params:
        laparams.word_margin = float(params['laparams_word_margin'])
    if 'laparams_boxes_flow' in params:
        laparams.boxes_flow = float(params['laparams_boxes_flow'])
    imagewriter=None
    with open(xml_path, 'wb') as outfp:
        device = XMLConverter(rsrcmgr, outfp, codec=codec, laparams=laparams,
                              imagewriter=imagewriter)
        pagenos = set()
        maxpages = 0
        password = ''
        for fname in (pdf_path,):
            with open(fname, 'rb') as fp:
                interpreter = PDFPageInterpreter(rsrcmgr, device)
                for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages,
                                              password=password, caching=True,
                                              check_extractable=True):
                    interpreter.process_page(page)
        device.close()
    return

def extract_xml_to_json(xml_path, json_path, params):
    # Define functions
    class CheckLoc():
        def __init__(self, page, relative):
            self.relative = relative
            self._page_limits = self._get_page_limits(page)

        def _get_page_limits(self, page):
            bboxes = [ list(map(float, l.attrib['bbox'].split(',')))
                         for l in page.iterdescendants('textline') ]
            b0, b1, b2, b3 = zip(*bboxes)
            return min(b0), min(b1), max(b2), max(b3)
    
        def get_rel(self, x, axis, x_is_distance=False):
            return ( (x if x_is_distance else x - self._page_limits[axis])
                     / (self._page_limits[axis+2] - self._page_limits[axis]) )

        def __call__(self, loc, limits, axis=None):
            if self.relative:
                assert axis in (0, 1), 'relative requires axis in (0,1)'
                loc = self.get_rel(loc, axis)
            return ( (True if limits[0] is None else (limits[0] < loc))
                     and (True if limits[1] is None else (loc < limits[1])) )
    
    def clean_page(p, check_loc, garbage_re=None, garloc=None, rel_loc=None):
        content = []
        garbage = []
        pnum = int(p.attrib['id'])
        for l in p.iterdescendants('textline'):
            bbox = list( map(float, l.attrib['bbox'].split(',')) )
            text = ''.join([t.text if t.text is not None else '_' for t in l])
            font = l[0].attrib['font']
            size = l[0].attrib['size']
            is_gartxt = bool( garbage_re and re.match(garbage_re, text) )
            is_garloc = garloc and ( check_loc(bbox[1], garloc['vtop'], 1) or\
                                     check_loc(bbox[1], garloc['vbottom'], 1) )
            thing = dict(bbox=bbox, text=text, pnum=pnum,
                         font=font, size=size, psec=[])
            if check_loc.relative:
                thing['rbbox'] = [check_loc.get_rel(x, i%2) for i, x in enumerate(bbox)]
            if is_gartxt or is_garloc:
                garbage.append( [thing, [is_gartxt, is_garloc] ] )
                vprint( u'Garbage (t/l) {}: {}\n'.format((is_gartxt, is_garloc), thing), 2 )
            else:
                content.append(thing)
        pbox = list( map(float, p.attrib['bbox'].split(',')) )
        return {'pnum': pnum, 'pbox': pbox, 'content': content, 'garbage': garbage}

    def columnize_page(page, flexible=0):
        content = page['content']
        pbox = page['pbox']
        hecoords = [l['bbox'][2] for l in content]
        hecoords.sort()
        hbcoords = [l['bbox'][0] for l in content]
        hbcoords.sort(reverse=True)
        div = (pbox[2],pbox[2])
        minhits = [len(content),len(content)]
        for he in hecoords:
            for hb in hbcoords:
                if not flexible and (hb > he) and (hb-he) > (div[1]-div[0]):
                    if not any( thing['bbox'][0]<hb and he<thing['bbox'][2]
                                  for thing in content ):
                         div = (he, hb)
                elif flexible and (hb > he) and ((hb-he)>0.01*(pbox[2]-pbox[0])):
                    hits=[0,0]
                    for thing in content:
                        if thing['bbox'][0]<hb and he<thing['bbox'][2]:
                            hits[1]+=1
                            if he<thing['bbox'][0]<hb:
                                hits[0]+=1
                    if max(hits) > flexible:
                        continue
                    if hits<minhits or (hits==minhits and (hb-he)>(div[1]-div[0])):
                        div = (he, hb)
                        minhits = hits
                        vprint('page {}; div {}; hits {}'.format(page['pnum'], div, hits))
        for thing in content:
            thing['psec'].append(0 if thing['bbox'][0]<div[1] else 1)

    def row_page(page):
        content = page['content']
        pbox = page['pbox']
        hecoords = [l['bbox'][3] for l in content]
        hecoords.sort()
        hbcoords = [l['bbox'][1] for l in content]
        hbcoords.sort(reverse=True)
        mid = (pbox[3]+pbox[1])/2
        div = (pbox[1],pbox[1])
        for he in hecoords:
            for hb in hbcoords:
                if (hb > he) and (hb-he) > (div[1]-div[0]):
                    if not any( thing['bbox'][1]<hb and he<thing['bbox'][3]
                                  for thing in content ):
                         div = (he, hb)
                         vprint('page {}; div {}'.format(page['pnum'], div))
#        if div[1]-div[0] < 10: div=(pbox[1],pbox[1])
        if abs( (div[1]+div[0]) - mid ) < mid*.4 : div=(pbox[1],pbox[1])
        for thing in content:
            thing['psec'].append(1 if thing['bbox'][1]<div[1] else 0)

    def check_separator(thing, check_loc, separator_re=None, beparator_re=None,
                        feparator=None, seploc=None, sepcharwidth=None):
        is_seploc = not seploc or (
            ( check_loc(thing['bbox'][0], seploc['hleft'], 0)
              or check_loc(thing['bbox'][0], seploc['hright'], 0) )
            and
            ( check_loc(thing['bbox'][1], seploc['vtop'], 1)
              or check_loc(thing['bbox'][1], seploc['vbottom'], 1) ) )
        is_sepfont = ( feparator
                       and re.match(feparator['font_re'], thing['font'])
                       and float(thing['size']) >= feparator['size'] )
        is_sepcharwidth = not sepcharwidth or (
            check_loc.get_rel(thing['bbox'][2] - thing['bbox'][0], 0, True)
            / (len(thing['text'].strip('\n'))
               - thing['text'].count('1')/4
               + thing['text'].count('0')/5)
            >= sepcharwidth )

        is_fseptxt = ( is_sepfont and ('s_re' in feparator)
                       and re.match(feparator['s_re'], thing['text']) )
        fbepmatch = ( is_sepfont and ('b_re' in feparator)
                    and re.match(feparator['b_re'], thing['text']) )
        is_septxt = separator_re and re.match(separator_re, thing['text'])
        bepmatch = beparator_re and re.match(beparator_re, thing['text'])

        if is_seploc and is_sepcharwidth:
            if is_fseptxt:
                return (thing, [])
            elif fbepmatch:
                thong = thing.copy()
                thing['text'] = fbepmatch.group(0) +'\n'
                thong['bbox'][0] += 1
                thong['text'] = thong['text'][fbepmatch.end():]
                return (thing, [thong])
            elif is_septxt:
                    return (thing, [])
            elif bepmatch:
                thong = thing.copy()
                thing['text'] = bepmatch.group(0) +'\n'
                thong['bbox'][0] += 1
                thong['text'] = thong['text'][bepmatch.end():]
                return (thing, [thong])
        return thing

    def collect_abstracts(a, page, check_loc, separator_re=None, beparator_re=None,
                          feparator=None, seploc=None, sepcharwidth=None):
        getsortkey = lambda x: (x['pnum'],x['psec'],-x['bbox'][1],x['bbox'][0])
        content = sorted(page['content'], key=getsortkey)
        for thing in content:
            r = check_separator(thing, check_loc, separator_re, beparator_re,
                                feparator, seploc, sepcharwidth)
            if isinstance(r, dict):
                a[-1]['abstract'].append(r)
            else:
                a.append( dict(separator=r[0], abstract=r[1]) )

    def collect_abstracts_by_geometry(a, page, separator_re=None, beparator_re=None,
                                      feparator=None, seploc=None, sepcharwidth=None):
        row_page(page)
        getsortkey = lambda x: (x['pnum'],x['psec'],-x['bbox'][1],x['bbox'][0])
        content = sorted(page['content'], key=getsortkey)
        sector_content = dict()
        for thing in content:
            sector_content.setdefault( tuple(thing['psec']), [] ).append(thing)
        vprint(sector_content.keys())
        for sec in (0,0), (0,1), (1,0), (1,1):
            sep = []
            abst = []
            iterator = iter(sector_content.setdefault(sec,[]))
            for thing in iterator:
                r = check_separator(thing, check_loc, separator_re,
                                    beparator_re, feparator, seploc,
                                    sepcharwidth)
                if type(r) is tuple:
                    sep.extend(r[0])
                    abst.extend(r[1])
                    break
                else:
                    abst.append(thing)
            for thing in iterator:
                abst.append(thing)
            a.extend( [ sep, abst ] )

    # Populate lxml etree from file
#    parser = etree.XMLParser(recover=True)
#    tree = etree.parse(xml_path, parser=parser)
#    root = tree.getroot()
    illegal_xml_chars_RE = re.compile(u'[\x00-\x08\x0b\x0c\x0e-\x1F\uD800-\uDFFF\uFFFE\uFFFF]')
    with open(xml_path) as f:
        cleantext = ( illegal_xml_chars_RE.sub('_', l) for l in f.readlines() ) # TODO why readlines ? juter iterate it!
        root = etree.fromstringlist(cleantext)

    # Remove download watermark
    for c in root.iterdescendants('textline'):
        if \
          len(c) == 2 and len(c[0].text) == 1 \
          and c[1].attrib == {} and c[1].text == '\n':
            c.getparent().remove(c)

    # Remove pdfminer extra spaces (obsolete: now suppressed at conversion)
    # for i in root.iterdescendants('text'):
    #     if i.attrib == {} and i.text == ' ':
    #         i.getparent().remove(i)

    # Sectorize page and collect abstracts
    abstracts = [dict(separator=None, abstract=[])]
    geometry = False
    key_path = os.path.sep.join(xml_path.split(os.path.sep)[1:])
    pages = filter( lambda x: int(x.attrib['id']) not in params['ignore_pages'][key_path],
                    root.iterdescendants('page'))
    for p in pages:
        check_loc = CheckLoc(p, params['loc_relative'])
        page = clean_page (p, check_loc, params['garbage_re'], params['garloc'])
        columnize_page(page, params['sector_flexible'])
        if geometry:
            collect_abstracts_by_geometry(abstracts, page, params['separator_re'],
                                          params['beparator_re'], params['feparator'],
                                          params['seploc'], params['sepcharwidth'])
        else:
            collect_abstracts(abstracts, page, check_loc, params['separator_re'],
                              params['beparator_re'], params['feparator'],
                              params['seploc'], params['sepcharwidth'])
    if not geometry:
        abstracts = extract_xml_to_json_exceptions(abstracts, params['exceptions'],
                                               json_path, params['separator_re'])
    # Save to file
    with open(json_path, 'w') as absfile:
        json.dump(abstracts, absfile, indent=4, sort_keys=True)

def extract_xml_to_json_exceptions(a, exceptions, json_path, separator_re):
    # Define exceptions
    def fix_early_content(a):
        for i, sa in enumerate(a[:-1]):
            while sa['abstract'] and \
                  abs(sa['abstract'][-1]['bbox'][1] - a[i+1]['separator']['bbox'][1]) < 5.0:
                thing = sa['abstract'].pop()
                a[i+1]['abstract'].insert(0, thing)
                vprint('Early content at {}: {}'.format(json_path, thing), 1)
    def remove_twocolumn_titles(a, size):
        for i, sa in enumerate(a):
            while sa['abstract'] and sa['abstract'][-1]['text'].isupper() and \
                  float(sa['abstract'][-1]['size']) > size:
                thing = sa['abstract'].pop()
                vprint('Removing section title at {}: {}'.format(json_path, thing), 1)
    def slice_markers(a, markers):
        for mark, er in markers:
            items = [ (i, j, sa, thing) for i, sa in enumerate(a)
                                      for j, thing in enumerate(sa['abstract'])
                                        if mark in thing['text'] ]
            assert len(items) == 1 , (mark, er)
            i, j, sa, thing = items[0]
            new_sa = dict(separator=None, abstract=None)
            new_sa['separator'] = dict(thing)
            new_sa['separator']['text'] = er+'\n'
            thing['text'] = thing['text'][ thing['text'].index(mark): ]
            new_sa['abstract'] = sa['abstract'][j:]
            sa['abstract'] = sa['abstract'][:j]
            a.insert(i+1, new_sa)
    def jump_markers(a, markers):
        for mark in markers:
            items = [ (i, j, sa, thing) for i, sa in enumerate(a)
                                      for j, thing in enumerate(sa['abstract'])
                                        if mark in thing['text'] ]
            assert len(items) == 1 , (mark)
            i, j, sa, thing = items[0]
            a[i+1]['abstract'].insert(0,thing)
            sa['abstract'].pop(j)

    # Treat exceptions, order matters.
    key_path = os.path.sep.join(json_path.split(os.path.sep)[1:])
    if 'slice_markers' in exceptions and key_path in exceptions['slice_markers']:
        vprint ( 'Processing slice_markers at {}\n'.format(json_path), 1 )
        markers = exceptions['slice_markers'][key_path]
        slice_markers(a, markers)
    if 'fix_early_content' in exceptions:
        vprint ( 'Processing fix_earlycontent at {}\n'.format(json_path), 1 )
        fix_early_content(a)
    if 'remove_twocolumn_titles' in exceptions:
        vprint ( 'Processing remove_twocolumn_titles at {}\n'.format(json_path), 1 )
        remove_twocolumn_titles(a, exceptions['remove_twocolumn_titles'])
    if 'jump_markers' in exceptions and key_path in exceptions['jump_markers']:
        vprint ( 'Processing jump_markers at {}\n'.format(json_path), 1 )
        markers = exceptions['jump_markers'][key_path]
        jump_markers(a, markers)
    
    return a


def extract_html(html_path, content_path, params):
    vprint('Converting {} ...'.format(html_path))
    doc = html.parse(html_path).getroot()
    multiple = bool( doc.cssselect('.sub-article') )
    doc_doi = doc.cssselect('.slug-doi')[0].text_content()
    doc_cat = {}
    e_cat = doc.cssselect('#cb-art-cat')
    assert(len(e_cat)==1)
    for i in e_cat[0].xpath('./ol/li/ul/li'):
        cat = i.xpath('./a')[0].text if i.xpath('./a') else i.text
        doc_cat[cat] = [j.text_content() for j in i.xpath('./ul/li')]
        if multiple and not doc_cat[cat]:
            doc_cat[cat]=doc.cssselect('.article>h1')[0].text_content()
    doc_abstracts = []
    for eabs in doc.cssselect('.sub-article' if multiple else '.article'):
        abstract = {}
        abstract['number']=eabs.xpath('./h1')[0].text
        abstract['title']=eabs.xpath('./h1/br')[0].tail
        abstract['authors']= dict()
        xaa = eabs.xpath('./div[1]/ol')
        if len(xaa):
            el_aut, el_aff = eabs.xpath('./div[1]/ol')
        else:
            continue # Skip abstract if no authors found
        affiliations = dict(
                         (i.xpath('./a')[0].attrib['name'],
                          i.xpath('.//span[@class="addr-line"]')[0].text_content()
                            if i.xpath('.//span[@class="addr-line"]')
                            else i.xpath('.//sup')[0].tail
                            if i.xpath('.//sup')
                            else i.xpath('.//address')[0].text_content())
                           for i in el_aff )
        authors = []
        for ea in el_aut.xpath('./li'):
            item = ea.xpath('.//span[1]')[0]
            name = ('__{}__ '.format(item.attrib['class'])
                      if item.attrib['class']!='name' else '') \
                   + item.text_content()
            tags = [i.attrib['href'][1:] for i in ea.xpath('.//*[@class="xref-aff"]')]
            authors.append( (name, tags) )
        for count, a in reversed( list( enumerate(authors) ) ):
            if not a[1]:
                if count == len(authors)-1:
                    abstract['authors'][a[0]]=list(affiliations.values())
                else:
                    abstract['authors'][a[0]]=abstract['authors'][authors[count+1][0]]
            else:
                try:
                    abstract['authors'][a[0]]=[ affiliations[i] for i in a[1] ]
                except Exception as err:
                    print ('\n\n', html_path, a, '\n' )
                    raise err
        abstract['text']=html.tostring( eabs.xpath('./div[2]')[0], encoding='unicode' )
        doc_abstracts.append(abstract)
    
    content = dict(
        doi = doc_doi,
        categories = doc_cat,
        abstracts = doc_abstracts,
        multiple = len(doc_abstracts)>1 )
    
    # Save to file
    with open(content_path, 'w') as contentfile:
        json.dump(content, contentfile)


# classes
class EsmoConference:
    def __init__(self, year, number, volume, supplement, flags, params,
                 save_dir, processes=None):
        # conference info
        self.year = str(year)
        self.number = str(number)
        self.volume = str(volume)
        self.supplement = str(supplement)
        self.flags = tuple(flags)
        # conference directories
        self.dirname = os.path.join(str(save_dir), self.year)
        self.metadir = os.path.join(self.dirname, 'meta')
        self.filedir = os.path.join(self.dirname, 'files')
        self.xmldir = os.path.join(self.dirname, 'xml')
        self.contentdir = os.path.join(self.dirname, 'content')
        self.printdir = os.path.join(self.dirname, 'print')
        os.makedirs(self.metadir, exist_ok=True)
        os.makedirs(self.filedir, exist_ok=True)
        os.makedirs(self.xmldir, exist_ok=True)
        os.makedirs(self.contentdir, exist_ok=True)
        os.makedirs(self.printdir, exist_ok=True)
        # manually calibrated parameters
        self.params = params
        # execution options
        self.processes = processes

    def __repr__(self):
        return '\t'.join([self.year, self.number, self.volume,
                         self.supplement, str(self.flags)])
    
    def get_toc_url(self):
        url = "https://annonc.oxfordjournals.org/" +\
              "content/" + self.volume +\
              "/suppl_" + self.supplement +\
              ".toc"
        return url
    
    def get_search_url(self, tocsectionid=None, firstindex=None, hits=None):
        url = "https://annonc.oxfordjournals.org/search?submit=yes" +\
              "&volume=" + self.volume +\
              "&issue=suppl_" + self.supplement +\
              ("?tocsectionid=" + tocsectionid if tocsectionid else '') +\
              ("&FIRSTINDEX=" + str(firstindex) if firstindex else '') +\
              ("&hits=" + hits if hits else '')
        return url
        
    def save_toc (self):
        url = self.get_toc_url()
        toc_path = os.path.join(self.dirname,'toc')
        if not os.path.exists(toc_path):
            content = urlopen(url).read()
            with open(toc_path, 'wb') as f:
                f.write(content)

    def walk_search(self, action, **kwargs):
        search_index = 0
        url = self.get_search_url()
        while url:
            vprint('Search url:\n' + url + '\n')
            content = urlopen(url).read()
            doc = html.document_fromstring(content)
            doc.make_links_absolute(url)
            next = doc.find_class('next-results-link')
            url = next[0].attrib['href'] if len(next) else ''
            search_index = action(doc, search_index, **kwargs)
            if search_index == -1:
                break

    def save_search_items(self, doc, search_index, all_of_them=False, download=False):
        items = doc.cssselect('li.results-cit')
        for item in items:
            for br in item.cssselect("br"):
                br.tail = "\n" + br.tail if br.tail else "\n"
            e = item.cssselect('div.cit-metadata span.cit-section')[0]
            section = e.text_content().strip(' :')
            vprint(section)
            e = item.cssselect('div.cit-metadata span.cit-title')[0]
            ees = e.cssselect('span.cit-sub-article-title')
            for ee in ees:
                ee.drop_tree()
            title = e.text_content().strip(' :')
            vprint(title)
            e = item.cssselect('div.cit-extra li.first-item a')[0]
            link = e.attrib['href']
            link = link.replace('+html', '', 1)
            vprint( link.partition('?')[0] )
            vprint('\n')
            self.save_item(link, section, title, search_index, download)
            search_index += 1
            if search_index > 16 and not all_of_them:
                search_index = -1
                break
        return search_index

    def save_item (self, url, section=None, title=None, search_index=None, download=False):
        name = os.path.basename(url).partition('?')[0]
        meta_path = os.path.join(self.metadir, name + '.json')
        file_path = os.path.join(self.filedir, name)
        if not os.path.exists(meta_path):
            content = {'section':section,'title':title,'search_index':search_index}
            with open(meta_path, 'w') as f:
                json.dump(content, f)
        if download and not os.path.exists(file_path):
            content = urlopen(url).read()
            with open(file_path, 'wb') as f:
                f.write(content)

    def all_pdf_to_tess(self):
        pool = Pool(processes=self.processes)
        ws = set()
        names = (n for n in os.listdir(self.filedir) if n.endswith('.pdf'))
        for name in names:
            pdf_path = os.path.join(self.filedir, name)
            tess_path = os.path.join(self.xmldir, name.rpartition('.')[0])
            if not os.path.exists(tess_path + '.pdf'):
                vprint('Converting {} ...'.format(tess_path))
                w = pool.apply_async(
                    convert_pdf_to_tess, (pdf_path, tess_path, self.params),
                    callback=lambda r, x=tess_path: vprint('... {} done!'.format(x)) )
                ws.add(w)
        while ws:
            time.sleep(0.5)
            ws.difference_update( [w for w in ws if w.ready() and (w.get() or True)] )
        pool.close(); pool.join()
    
    def all_pdf_to_xml(self, pdfdir=None):
        if pdfdir is None:
            pdfdir = self.filedir
        pool = Pool(processes=self.processes)
        ws = set()
        names = (n for n in os.listdir(pdfdir) if n.endswith('.pdf'))
        for name in names:
            pdf_path = os.path.join(pdfdir, name)
            xml_path = os.path.join(self.xmldir, name.rpartition('pdf')[0] + 'xml')
            if not os.path.exists(xml_path):
                vprint('Converting {} ...'.format(xml_path))
                w = pool.apply_async(
                    convert_pdf_to_xml, (pdf_path, xml_path, self.params),
                    callback=lambda r, x=xml_path: vprint('... {} done!'.format(x)) )
                ws.add(w)
        while ws:
            time.sleep(0.5)
            ws.difference_update( [w for w in ws if w.ready() and (w.get() or True)] )
        pool.close(); pool.join()

    def all_xml_to_json(self):
        pool = Pool(processes=self.processes)
        ws = set()
        names = (n for n in os.listdir(self.xmldir) if n.endswith('.xml'))
        ignore = re.compile(self.params['ignore_files']+'$') \
                if self.params['ignore_files'] else None
        for name in names:
            xml_path = os.path.join(self.xmldir, name)
            json_path = os.path.join(self.xmldir, name.rpartition('xml')[0] + 'json')
            if not os.path.exists(json_path) and not (ignore and ignore.search(xml_path)):
                vprint('Converting {} ...'.format(json_path))
                w = pool.apply_async(
                    extract_xml_to_json, (xml_path, json_path, self.params),
                    callback=lambda r, x=json_path: vprint('... {} done!'.format(x)) )
                ws.add(w)
        while ws:
            time.sleep(0.5)
            ws.difference_update( [w for w in ws if w.ready() and (w.get() or True)] )
        pool.close(); pool.join()
        vprint('XML to JSON conversion successful!')

    def all_html_to_content(self):
        pool = Pool(processes=self.processes)
        ws = set()
        names = (n for n in os.listdir(self.filedir) if n.endswith('.abstract'))
        for name in names:
            html_path = os.path.join(self.filedir, name)
            json_path = os.path.join(self.contentdir, name + '.json')
            if not os.path.exists(json_path):
                w = pool.apply_async(
                    extract_html, (html_path, json_path, self.params),
                    callback=lambda r, x=json_path: vprint('... {} done!'.format(x)) )
                ws.add(w)
        while ws:
            time.sleep(0.5)
            ws.difference_update( [w for w in ws if w.ready() and (w.get() or True)] )
        pool.close(); pool.join()
    
    def print_json(self, json_path, print_path=None):
        def writer(abstracts, f=sys.stdout):
            re_clean = re.compile(' \n(?=[^$])')
            name = os.path.basename(json_path)
            f.write('\n\n{}\n\n\n'.format(70*'-'))
            f.write('File {} contains {} abstracts.\n\n\n' \
                    .format(name, len(abstracts)-1 ))
            for a in abstracts[1:]:
                f.write( '\t' + re_clean.sub(' ', a['separator']['text']) + '\n\n\n')
                text = ''.join(j['text'] for j in a['abstract'])
                f.write( re_clean.sub(' ', text) + '\n\n\n')
            f.write('File {} contains {} abstracts.\n' \
                    .format(name, len(abstracts)-1 ))
        with open(json_path) as fj:
            abstracts = json.load(fj)
        if print_path:
            with open(print_path, 'w') as fp:
                writer(abstracts, fp)
        else:
            writer(abstracts)

    
    def print_abstracts(self):
        re_clean = re.compile(' \n(?=[^$])')
        for name in os.listdir(self.xmldir):
            if name.endswith('.json'):
                json_path = os.path.join(self.xmldir, name)
                self.print_json(json_path)
        for name in os.listdir(self.contentdir):
            if name.endswith('.json'):
                json_path = os.path.join(self.contentdir, name)
                with open(json_path) as f:
                    content = json.load(f)
                print('\n\n{}\n\n'.format(70*'-'))
                print('File {} contains {} abstracts.\n\n' \
                        .format(name, len(content['abstracts'])))
                print('doi:{}'.format(content['doi']))
                print('categories: {}'.format(content['categories']))
                for abstract in content['abstracts']:
                    print(u'\n\n{} - {}'.format(abstract['number'], abstract['title']))
                    print(u'{}\n'.format(abstract['authors']))
                    print(u'{}'.format(abstract['text']))
                print('\n\nFile {} contains {} abstracts.' \
                        .format(name, len(content['abstracts'])))


    def changes(self, accept=False, update=False, view=False, diff=False, inspect=False):
        if accept:
            if not [n for n in os.listdir(self.printdir) if n.endswith('.ctxt')]:
                vprint('Can\'t accept an empty changeset, run --changes first')
                return
            names = (n for n in os.listdir(self.printdir) if n.endswith('.txt'))
            for name in names:
                accep_path = os.path.join(self.printdir, name)
                os.remove(accep_path)
            names = (n for n in os.listdir(self.printdir) if n.endswith('.ctxt'))
            for name in names:
                print_path = os.path.join(self.printdir, name)
                accep_path = os.path.join(self.printdir,
                                          name.rpartition('ctxt')[0] + 'txt')
                os.rename(print_path, accep_path)
            vprint('Jolly good, changes accepted!')
        if update:
            names = (n for n in os.listdir(self.xmldir) if n.endswith('.json'))
            for name in names:
                json_path = os.path.join(self.xmldir, name)
                os.remove(json_path)
            self.all_xml_to_json()
            names = (n for n in os.listdir(self.printdir) if n.endswith('.ctxt'))
            for name in names:
                print_path = os.path.join(self.printdir, name)
                os.remove(print_path)
            names = (n for n in os.listdir(self.printdir) if n.endswith('.diff'))
            for name in names:
                diff_path = os.path.join(self.printdir, name)
                os.remove(diff_path)
            names = (n for n in os.listdir(self.xmldir) if n.endswith('.json'))
            for name in names:
                json_path = os.path.join(self.xmldir, name)
                print_path = os.path.join(self.printdir,
                                          name.rpartition('json')[0] + 'ctxt')
                accep_path = os.path.join(self.printdir,
                                          name.rpartition('json')[0] + 'txt')
                diff_path = os.path.join(self.printdir,
                                          name.rpartition('json')[0] + 'diff')
                self.print_json(json_path, print_path)
                if os.path.isfile(accep_path):
                    with open(diff_path, 'w') as f:
                        subprocess.run(['diff', '-c', accep_path, print_path],
                                         stdout=f)
                else:
                    vprint('No diff created as no accepted print found: {}'.format(print_path))
        if view:
            view_paths = [os.path.join(self.printdir, n)
                            for n in os.listdir(self.printdir)
                              if n.endswith('.txt') ]
            if inspect:
                insp_paths = [os.path.join(self.xmldir, n[:-4]+'.json')
                                for n in os.listdir(self.printdir)
                                  if n.endswith('.txt') ]
                view_paths = [p[ext] for p in zip(view_paths, insp_paths) for ext in (0, 1) ]
            if view_paths:
                subprocess.Popen(['gedit', '-s'] + view_paths, stderr=subprocess.DEVNULL)
            else:
                vprint('No accepted prints found, run --changes --accept first')
        if diff:
            diff_paths = [os.path.join(self.printdir, n)
                            for n in os.listdir(self.printdir)
                              if n.endswith('.diff')
                                 and os.path.getsize( os.path.join(self.printdir, n) ) ]
            if inspect:
                insp_paths = [os.path.join(self.xmldir, n[:-5]+'.json')
                                for n in os.listdir(self.printdir)
                                  if n.endswith('.diff')
                                     and os.path.getsize( os.path.join(self.printdir, n) ) ]
                diff_paths = [p[ext] for p in zip(diff_paths, insp_paths) for ext in (0, 1) ]
            if diff_paths:
                subprocess.Popen(['gedit', '-s'] + diff_paths, stderr=subprocess.DEVNULL)
            else:
                vprint('No diffs found.')


if __name__ == '__main__': sys.exit(main(sys.argv))
