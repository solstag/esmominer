#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# esmo.py -- script to extract and manipulate data from ESMO conferences
#
# Copyright (C) 2016-2016 Alexandre Hannud Abdo <abdo@member.fsf.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Meaning of params used in this file:

'flags' : 'dict, general human oriented information about a corpus'
'garloc' : 'two vertical intervals with None meaning boundless, remove everything located within these intervals'
'garbage_re' : 'regex, remove anything matching it'
'sector_flexible' : 'int, when sectorizing the page, accept up to this many elements crossing sectors'
'seploc' : 'two vertical and two horizontal intervals with None meaning boundless, look for separators only within the four intersections between the horizontal and vertical stripes'
'feparator' : 'font attributes and regexes, match against them to split content in blocks; s_re has precedence over b_re; b_re assumes the separator is followed by non-separator text in the same element'
'separator_re' : 'regex, if feparator doesn't match, try matching this without taking the font into account'
'beparator_re' : 'regex, like separator_re, tried in case that doesn't match, but assumes the separator is followed by non-separator text in the same element'
'ignore_files' : 'list of file names, may be used to skip processing tables of contents and similar stuff'
'ignore_pages' : 'dict from file names to lists of pages, useful to skip publicity pages'
'exceptions' : 'dict of options to apply exceptional parsing, see extract_xml_to_json_exceptions() in esmo.py'
"""

import sys
from collections import defaultdict

class EsmoDescription:
    def __init__(self):
        self.infos = [
            dict(year="2014", number="39", volume="25", supplement="4",
                 flags=('html', 'native', 'individual')),
            dict(year="2012", number="37", volume="23", supplement="9",
                 flags=('html', 'native', 'grouped', 'sequential')),
            dict(year="2010", number="35", volume="21", supplement="8",
                 flags=('pdf', 'ocr', 'grouped', 'twocolumn')),
            dict(year="2008", number="33", volume="19", supplement="8",
                 flags=('pdf', 'ocr', 'grouped', 'twocolumn')),
            dict(year="2006", number="31", volume="17", supplement="9",
                 flags=('pdf', 'ocr', 'grouped', 'twocolumn')),
            dict(year="2004", number="29", volume="15", supplement="3",
                 flags=('pdf', 'img', 'grouped', 'twocolumn')),
            dict(year="2002", number="27", volume="13", supplement="5",
                 flags=('pdf', 'ocr', 'grouped', 'twocolumn')),
            dict(year="2000", number="25", volume="11", supplement="4",
                 flags=('pdf', 'ocr', 'grouped', 'twocolumn')),
            dict(year="1998", number="23", volume="9", supplement="4",
                 flags=('pdf', 'ocr', 'grouped', 'twocolumn')),
            dict(year="1996", number="21", volume="7", supplement="5",
                 flags=('pdf', 'ocr', 'grouped', 'twocolumn')),
            dict(year="1994", number="19", volume="5", supplement="8",
                 flags=('papier', 'img', 'grouped', 'twocolumn')),
            dict(year="1992", number="17", volume="3", supplement="5",
                 flags=('pdf', 'ocr', 'grouped', 'quadrants')),
            dict(year="1990", number="15", volume="1", supplement="1",
                 flags=('pdf', 'ocr', 'grouped', 'quadrants')),
        ]
    
    def __iter__(self):
        return iter( dict( params=self.get_params(i), **i )
                       for i in self.infos )
    
    def __repr__(self):
        title = "\n    ESMO Conferences from 1990 to 2014\n\n"
        header = "Year\tNumber\tVolume\tSupplement\tFlags\n"
        content = '\n'.join(
                    '\t'.join(['{}']*5).format(
                        *(d[x] for x in header.lower().split()) )
                      for d in self.infos ) + '\n'
        return title + header + content
    
    def get_params(self, info):
        params = {}
        
        # Defaults (other keys should not have defaults)
        params['flags'] = info['flags']
        params['garloc'] = None
        params['garbage_re'] = None
        params['sector_flexible'] = 0
        params['separator_re'] = None
        params['beparator_re'] = None
        params['feparator'] = None
        params['seploc'] = None
        params['sepcharwidth'] = None
        params['ignore_files'] = None
        params['ignore_pages'] = defaultdict(list)
        params['loc_relative'] = False
        
        # Get values based on year
        if info['year'] == '1990':
            params['laparams_line_margin'] = float(1000)
            params['laparams_word_margin'] = sys.float_info.max
            params['ignore_files'] = 'xml/(131|133|iii)\.full\.xml'
            params['sector_flexible'] = sys.maxsize
            params['separator_re'] = '|'.join([
                '[PWV][\dILl][\dILl]?: ?[\dl]{1,3} ?$',
                'P2\.12',
                'PI: Haematologic malignancies$',
                'Wl: Growth factors$'])
            params['garloc'] = dict( vtop = (795.0,None), vbottom = (None,75.0) )
            params['garbage_re'] = '|'.join([
                '^Anna.*f ?Oncology, Supplement',
                '.*Printed in the Netherlands',
                '(.+)ESMO Congress [-—].',
                '[PWV] ?[\dIl]+: [\dIl]+[-\^] ?[\dIl]+$'])
            params['exceptions'] = dict(
                slice_markers = {
                    '1990/xml/79.full.json':
[('HIGH-DOSE THIO-TEPA (TT) AND EPIRUBICINE (EPI) PLUS RHU-GM-CSF', 'W1:11')],
                    '1990/xml/118.full.json':
[('PROGNOSTIC FACTORS IN FIGO STAGE III-IV OVARIAN CANCER PATIENTS', 'W11:1')],
                }
            )
             
        elif info['year'] == '1992':
            params['laparams_line_margin'] = 6.0
            params['laparams_char_margin'] = 10000.0
            params['laparams_word_margin'] = sys.float_info.max
            params['ignore_files'] = 'xml/(207|223|229|iii|v)\.full\.xml'
            params['sector_flexible'] = 25
            params['garbage_re'] = '|'.join([
                '^Anna.*f ?Oncology 3 [\(\{]Suppl',
                '.*Printed in the Netherlands'])
            params['garloc'] = dict( vtop = (788.5,None), vbottom = (None,75.0) )
            params['seploc'] = dict( hleft = (11.2,37.21), hright = (294.7,317.7),
                                    vtop = (760.2,777.0), vbottom = (403.5,419.5) )
            params['separator_re'] = '|'.join([
                u'[•]?[\dCOPQRSego]{3} ?$',
                'AGO ?$',
                u'• ?[J\d] ?\d\d$',
                '3g-\| ?$',
                'AC-7 ?$',
                '1-fC ?$',
                '5Q-J ?$',
                '-J-JO ?$',
                'J-\\\\-\\\\ ?$',
                'Q1$',
                'Q-1Q$',
                '71 $',
                'Q-\| ?(\d[\dQ] ?)?$',
                'Q[29Q] ?$',
                '7[gqQ] ?$',
                'KQ ?$',
                'ee ?$',
                u'5» ?$',
                'C[OC] ?$',
                'fil ?$',
                'g[5Qg] ?$',
                'Qfi ?$',
                'gfJ7 ?$',
                'OQfl ?$',
                u'•[J\\\\](-t)+ ?$',
                u'•IQI ?$',
                u'•JOT ?$',
                u'•IQC ?$',
                'igO ?$',
                'ICQ ?$',
                'HOC ?$',
                '[35]"[\|J][35] ?$',
                'C?-\|[\dJ]+ ?$',
                u'•\|?[\dJ]+ ?$',
                '\d\d $'])
            params['beparator_re'] = '|'.join([
                u'[•]?[\dCOPQRSego]{3} ',
                'AGO ',
                u'• ?[J\d] ?\d\d ?',
                '3g-\| ',
                'AC-7 ',
                '1-fC ',
                '5Q-J ',
                '-J-JO ',
                'J-\\\\-\\\\ ',
                'Q1 4 ',
                'Q-1Q ',
                'Q-\| ?(\d[\dQ] ?)?',
                'Q[29Q] ',
                '7[gqQ] ',
                'KQ ',
                'ee ',
                u'5» ',
                'C[OC] ',
                'CCf\) ',
                'RQ8fi ',
                'fil ',
                'g[5Qg]( |(?=[A-Z]))',
                'Qfi ',
                'gfJ7 ',
                'OQfl ',
                u'•[J\\\\](-t)+ ',
                u'•IQI ',
                u'•JOT ',
                u'•IQC ',
                'igO ',
                'ICQ ',
                'HOC ',
                '[35]"[\|J][35] ',
                'C?-\|[\dJ]+ ',
                u'•\|?[\dJ]+ ',
                '\d\d ',
                '\d{3} ?',
                u'[•]?\d\d(?=[A-Z])',
                '5(?=PRO)',
                'CCO(?=[A-Z])',
                'gQg(?=[A-Z])',
                '(71|75|76) $',
                '43$'])
            params['exceptions'] = dict(
                fix_early_content = None,
                jump_markers = {
                    '1992/xml/27.full.json':
[('HIGH EFFICACY OF AN INTENSIVE PREOPERATIVE CHEMO-RADIO-'),
 ('CISPUTH i vp-ie atnimft 5 DAYS nrosiai n hsmxs> WM-SMI CSJ, UK'),
 ('IFOSBAJCEDE (I), CISPLATIN (P) AND ETOPOSIDE (E) IN THE')],
                    '1992/xml/12.full.json':
[('HIGH EFFICACY OF AN INTENSIVE PREOPERATIVE CHEMO-RADIO-'),
 ('CISPUTH i vp-ie atnimft 5 DAYS nrosiai n hsmxs> WM-SMI CSJ, UK'),
 ('IFOSBAJCEDE (I), CISPLATIN (P) AND ETOPOSIDE (E) IN THE')],
                },
                slice_markers = {
                    '1992/xml/1.full.json':
[('ACCELERATED CHEMOTHERAPY WITH OM-CSF IN SMALL CELL LUNG','1'),
 ('THE USE OF G-CSF TO INCREASE TREATMENT','2'),
 ('Tumor Necrosis Factor (TNF) as an autocrlne growth','3'),
 ('EFFECTS OF RECOMBINANT HUMAN ERYTHROPOIETIN ON TRANSFUSION','4')],

                    '1992/xml/130.full.json':
[('Pharmacological properties of the novel multldrug resistance', '500'),
 ('THE EFFECT OF BENZENE ON P-CLYCOPROTEIN EXPRESSION IN', '504'),
 ('Enhancement of cisplatin antltumor activity in a human', '508'),
 ('AMPHOLIPOSOMES AS A POTENTIAL WAY OF REVERSING', '512')],

                    '1992/xml/143.full.json':
[('THE RELEVANCE OF THALLIUM-201 IMAGING IN CEREBRAL', '548')],

                    '1992/xml/153.full.json':
[('SECOND-LINE THERAPY WITH CARBOPLAT1NUM (C) AND', '622')],

                    '1992/xml/75.full.json':
[('SOHAIULIHE PLUS IAMOXIFEJ) IN POST hENOPAUSAL BREASI CANCER PATIENTS.', '293'),
 ('BETA-INTERFERON (B-IFN) AND MEDROXYPROGESTERONE ACETATE','306'),
 ('SERUM LEVEL ADAPTED HIGH-DOSE MEDROXYPROGESTERONE','305'),
 ('COMPARISON OF CONTINUOUS AND DISCONTINUOUS','307'),
 ('EXPERIENCE OF WEEKLY INTENSIVE INDUCTION CHEMOTHERAPY','313'),
 ('KTOXANTROKE PLUS G-CSF AS A DOSE INTENSIVE THERAPY','314'),
 ('PRELIMINARY DATA ON 4','319'),
 ('MITOMYCIN C, METHOTREXATE, MTTOXANTRONE','328'),
 ('WD EfeNMBIE','344')],

                    '1992/xml/46.full.json':
[('TOMAOXOGY Of 3JESIAN2 P ANAIOQES, HFIOTS G AC E:','191'),
 ('AN AREA UNDER THE CURVE (AUC) PHASE I STUDY OF','199'),
 ('Abstract Withdrawn Abstract Withdrawn','227'),
 ('Abstract Withdrawn Abstract Withdrawn','229')
 ],
 
                    '1992/xml/164.full.json':
[('DISSEMINATED CARCINOMATOSIS OF THE BO^E HARROW(','654')],

                    '1992/xml/27.full.json':
[('INTRAARTERIAL HEPATIC CHEMOTHERAPY (IHAC) FOR', '81'),
 ('BETTER OUTLOOK FOR PATIENTS WITH NON-SMALL CELL LUNG', '105'),
 ('LOCAL nawDiAHCw AUIWWH) tern oaeiNAiicN OBOMKAFY', '113'),
 ('FLUOROURACIL (FU) / FOLINIC ACID (FA) / VTNORELBINE (VNB) /', '122'),
 ('Crrcadian chemotherapy against stage IIIB-IV non-unall-cj', '129'),
 ('PHASE U TRIAL OF CAKBOPLATIN, IFOSFAMTOE AND', '132'),
 ('ANAEMIA AND PROGNOSIS IN LUNG CANCER - G. BERTHIOT', '133'),
 ('CYCLOPHOSPHAMIDE.EPIRUBICIN AND CISPLATIN AS FIRST LINE -J 35', '134'),
 ('ACTIVITY OF FOTEMU9TINE - CISPLATIN (CDDP)', '135'),
 ('MITOMYCINE, IFOSFAMIDE, CISPLATIN (MIC) as', '139'),
 ('5MALL CELL LUNG CANCER (SCLC) IN PATIENTS SURVI-', '144'),
 ('OrU rfosfairide In efcterty and/or imflt prterrM with tmall cell', '148'),
 ('PHASE II TRIAL OF PIRARUBICIN IN PREVIOUSLY TREATED','149'),
 ('A RANDOMISED TRIAL OF 3 VERSUS 6 CYCLES OF COMBINATION', '154')],
   
                    '1992/xml/12.full.json':
[('INTRAARTERIAL HEPATIC CHEMOTHERAPY (IHAC) FOR', '81'),
 ('BETTER OUTLOOK FOR PATIENTS WITH NON-SMALL CELL LUNG', '105'),
 ('LOCAL nawDiAHCw AUIWWH) tern oaeiNAiicN OBOMKAFY', '113'),
 ('FLUOROURACIL (FU) / FOLINIC ACID (FA) / VTNORELBINE (VNB) /', '122'),
 ('Crrcadian chemotherapy against stage IIIB-IV non-unall-cj', '129'),
 ('PHASE U TRIAL OF CAKBOPLATIN, IFOSFAMTOE AND', '132'),
 ('ANAEMIA AND PROGNOSIS IN LUNG CANCER - G. BERTHIOT', '133'),
 ('CYCLOPHOSPHAMIDE.EPIRUBICIN AND CISPLATIN AS FIRST LINE -J 35', '134'),
 ('ACTIVITY OF FOTEMU9TINE - CISPLATIN (CDDP)', '135'),
 ('MITOMYCINE, IFOSFAMIDE, CISPLATIN (MIC) as', '139'),
 ('5MALL CELL LUNG CANCER (SCLC) IN PATIENTS SURVI-', '144'),
 ('OrU rfosfairide In efcterty and/or imflt prterrM with tmall cell', '148'),
 ('PHASE II TRIAL OF PIRARUBICIN IN PREVIOUSLY TREATED','149'),
 ('A RANDOMISED TRIAL OF 3 VERSUS 6 CYCLES OF COMBINATION', '154')],

                    '1992/xml/114.full.json':
[('CLINICAL AND NEUROPHYSIOLOGIC EVALUATION OF HIGH-DOSE','443'),],

                    '1992/xml/92.full.json':
[('AWUFJUIULN M© WXSE ETOOCGIS IN m/PtCED EFEA3T','361'),],

                    '1992/xml/175.full.json':
[('Opiold narcotic consumption in anticancer center,','672'),
 ('RANDOMISED, DOUBLE-BUND, CROSS-OIVER TRIAL OF 1HK5HT, ANTAGONIST', '700')],
 
                    '1992/xml/117.full.json':
[('CONCORDANT PERIPHERAL BLOOD AND BONE MARROW bCl-2','451'),
 ('A SIX WEEK CHEMOTHERAPY REGIMEN FOR RELAPSED','485')],
 
                    '1992/xml/193.full.json':
[('DISCOURS DES USAGERS ET DES PARTENAIRES','742'),
 ('PRESENTATION OR ALE.REINSERTION.','744'),
 ("SUIV'I SOCIAL DE PATIENTS CANCEREUX SANS DOMICILE",'743'),
 ('PSYCHOSOCIAL ASPECTS OF LUNG CANCER - EXPERIENCES FROM A', '750')],
 
                    '1992/xml/190.full.json':
[('PERTE SANGUINE DUE AUX EXAMENS BIOLOGIQUES AU','731')],

                    '1992/xml/200.full.json':
[('COMBINATION CHEMOTHERAPY FOR BLADDER CANCER', '910'),
 ('CARBOPLATIR IH COMBINATION AS FIRST LINE THERAPY', '911')],
                }
            )
                
        elif info['year'] == '1994':
            params['ignore_files'] = 'xml/Index_of.*\.xml'
            params['dpi'] = 300 # 70 300
            params['loc_relative'] = True
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology 5 ?[\(\{]Supp',
                '.*Printed in the (Nether|\w+lands)'])
            params['garloc'] = dict( vtop = (.985, None), vbottom = (None, .01) )
            params['seploc'] = dict( hleft = (None, 0.03), hright = (.5, .53),
                                    vtop = (None,None), vbottom = (None,None) )
            params['sepcharwidth'] = 0.0073 # 73
            params['separator_re'] = '|'.join([
                'P8$', '021$', 'P89$', '210$', '281$', '285$',
            ])
#            params['beparator_re'] = '|'.join([
#                '[\|1]? ?\d{2,3} ?[OP0]? ?([\|]|[\[I] ) ?',
#                '[\[\]\|I] ?\d{3}[OP0]? ?([\|]|[\[I] )? ?',
#                '\d{3}[OP](?=\w)'])
            params['feparator'] = dict(
                font_re = 'GlyphLessFont',
                # Size depends on DPI. Weirdly enough, setting a lower DPI in
                # an image sometimes yields better results.
                size = {300:7.9, 70:32.0}[params['dpi']],
                s_re = '|'.join([
                    '(1 )?[OPDNI0]{0,2}1?\d{1,3}$',
                ]),
                b_re = '|'.join([
                   '\d{2,4} (?=[A-Z])',
                   '[OPDNI0]\d{1,4} (?=[A-z\d])(?!m)',
                   '(1 )?[OPDNI0]{0,2} ?\d{2,3} (?=[0-9"”]{0,3}[l\|-]?[A-Zn%])',
                ]) )
            params['exceptions'] = dict(
                remove_twocolumn_titles = {300:8.5, 70:34.0}[params['dpi']],
                fix_early_content = None,
                slice_markers= {
                    '1994/xml/Section_1:_Plenary_Lectures-converted.json':
[('Mature Results of a Prospective Randomized Trial','03')],
                    '1994/xml/Section_3:_Bench_to_bedsides-converted.json':
[('Microvessel Density, Endothelial Cell Proliferation and','025')],
                    '1994/xml/Section_7:_Colorectal_cancer-converted.json':
[('Weekly High-Dose Continuous Infusion 5-Fluorouracil,','275')],
                    '1994/xml/Section_9:_Genito-urinary_tumor-converted.json':
[('Seconday Tumours F°"°W1n9 Et°P°51de Containing','P334'),
 ('The Testicular Function in Patients with Testicular','P335')],
                    '1994/xml/Section_19:_Melanoma_and_sarcoma-converted.json':
[('Complex Treatment Methods in Osteosarcoma Patients','896')],
                }
            )
             
        elif info['year'] == '1996':
            params['laparams_char_margin'] = 1.0
            params['laparams_word_margin'] = sys.float_info.max
            params['ignore_files'] = 'xml/(2|147|163|iii|iv|ix|v|vi|x|xi)\.full\.xml'
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology 7 ?[\(\{]Suppl',
                '.*Printed in the Netherlands'])
            params['garloc'] = dict( vtop = (795.0,None), vbottom = (None,65.0) )
            params['seploc'] = dict( hleft = (None,90.), hright = (290.0,327.0),
                                    vtop = (None,None), vbottom = (None,None) )
            params['feparator'] = dict(
                font_re='Helvetica-Bold|Times-Roman|Helvetica|Times-Bold',
                size=9.01,
                s_re= '|'.join([
                    '[\[\|1]? ?\d{2,4} ?([OPDNI]{1,2})? ?([\|\[I])? ?$',
                    ]),
                b_re= '|'.join([
                    '[\]\[\|1I]? ?\d{2,4}( ?[OPDNI0]{0,2})? ?[\|\[I1] ?(?=[A-Z\d])',
                    '([\|]? ?|1 )?\d{3} ?[OPDNI0]{0,2} ?([\|\[I])? ?(?=[A-Z])',
                    ]) )
            params['ignore_pages']['1996/xml/18.full.xml'] = [2]
            params['ignore_pages']['1996/xml/11.full.xml'] = [3,4]
            params['exceptions'] = dict(
                remove_twocolumn_titles = 9.5,
                fix_early_content = None,
                slice_markers= {
                    '1996/xml/62.full.json':
[('The Effect of r h TNF-cc on Morphological Changes of','31OP')],
                    '1996/xml/109.full.json':
[('Regarding Real Utility of','555')],
                }
            )
        
        elif info['year'] == '1998':
            params['laparams_char_margin'] = 1.0
            params['laparams_word_margin'] = sys.float_info.max
            params['ignore_files'] = 'xml/(153|169|v|vi|vii|viii|xi)\.full\.xml'
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology, Supplement \d',
                '.*Printed in the Netherlands'])
            params['garloc'] = dict( vtop = (785.0,None), vbottom = (None,60.0) )
            params['seploc'] = dict( hleft = (None,90.), hright = (290.0,327.0),
                                    vtop = (None,None), vbottom = (None,None) )
            params['separator_re'] = '|'.join([
                '[\|1]?\d{3}[OP0]? ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP] ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP]? ?([\|\[I]) ?$',
                '51$'])
            params['beparator_re'] = '|'.join([
                '[\|1]? ?\d{2,3} ?[OP0]? ?([\|]|[\[I] ) ?',
                '[\[\]\|I] ?\d{3}[OP0]? ?([\|]|[\[I] )? ?',
                '\d{3}[OP](?=\w)',
                '\| 12O\) '])
            params['feparator'] = dict(font_re='Helvetica-Bold',
                                      size=8.01,
                                      s_re='\d{2,3} ?([OP] )?$',
                                      b_re='\d{3} ?([OP])? ?(?=\w)')
            params['exceptions'] = dict(
                remove_twocolumn_titles = 9.5,
                fix_early_content = None )
        
        elif info['year'] == '2000':
            params['laparams_char_margin'] = 1.0
            params['laparams_word_margin'] = sys.float_info.max
            params['ignore_files'] = 'xml/(3|43|157|175|195|ix|v|vi|vii|xiv|xiv-b)\.full\.xml'
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology, Supplement \d',
                '.*Printed in [Tt]he Netherlands'])
            params['garloc'] = dict( vtop = (785.0,None), vbottom = (None,60.0) )
            params['seploc'] = dict( hleft = (None,90.), hright = (290.0,327.0),
                                    vtop = (None,None), vbottom = (None,None) )
            params['separator_re'] = '|'.join([
                '[\|1]?\d{3}[OP0]? ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP] ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP]? ?([\|\[I]) ?$'])
            params['beparator_re'] = '|'.join([
                '[\|1]? ?\d{2,3} ?[OP0]? ?([\|]|[\[I] ) ?',
                '[\[\]\|I] ?\d{3}[OP0]? ?([\|]|[\[I] )? ?',
                '\d{3}[OP](?=\w)'])
            params['feparator'] = \
                dict(font_re='Helvetica-Bold',
                     size=8.01,
                     s_re='|'.join([
                         '[\|1]? ?\d{2,3} ?([OPDNI]{1,2})?([\|\[I])? ?$',
                     ]),
                     b_re='|'.join([
                        '[\|1]? ?\d{3}( ?[OPDNI0]{0,2})? ?[\|\[I1] ?(?=[A-Z])',
                        '([\|]? ?|1 )?\d{2,3} ?[OPDNI0]{0,2} ?([\|\[I])? ?(?=[A-Z])',
                     ]))
            params['exceptions'] = dict(
                remove_twocolumn_titles = 9.5,
                fix_early_content = None,
                slice_markers = {
                    '2000/xml/90.full.json':
[('Circulating extracellular DNA of head and neck', '402PD')]
                }
            )
        
        elif info['year'] == '2002':
            params['laparams_char_margin'] = 1.0
            params['laparams_word_margin'] = sys.float_info.max
            params['ignore_files'] = 'xml/(1|1-I|1-XXI)\.full\.xml'
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology, Supplement \d',
                '.*Printed in [Tt]he Netherlands'])
            params['garloc'] = dict( vtop = (740.0,None), vbottom = (None,0.0) )
            params['seploc'] = dict( hleft = (None,90.), hright = (290.0,327.0),
                                    vtop = (None,None), vbottom = (None,None) )
            params['separator_re'] = '|'.join([
                '[\|1]?\d{3}[OP0]? ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP] ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP]? ?([\|\[I]) ?$'])
            params['beparator_re'] = '|'.join([
                '[\|1]? ?\d{2,3} ?[OP0]? ?([\|]|[\[I] ) ?',
                '[\[\]\|I] ?\d{3}[OP0]? ?([\|]|[\[I] )? ?',
                '\d{3}[OP](?=\w)'])
            params['feparator'] = \
                dict(font_re='HOIJML\+TimesNRMT',
                     size=10.01,
                     s_re='|'.join([
                         '[\|1]? ?\d{1,3} ?([OPDNI]{1,2})?([\|\[I])? ?$',
                     ]),
                     b_re='|'.join([
                        '[\|1]? ?\d{3}( ?[OPDNI0]{0,2})? ?[\|\[I1] ?(?=[A-Z])',
                        '([\|]? ?|1 )?\d{2,3} ?[OPDNI0]{0,2} ?([\|\[I])? ?(?=[A-Z])',
                     ]))
            params['exceptions'] = dict(
                remove_twocolumn_titles = 9.5,
                fix_early_content = None )
        
        elif info['year'] == '2004':
            params['ignore_files'] = 'xml/(iii1|iii249|iii273)\.full\.xml'
            params['dpi'] = 150 # needs verification
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology, Supplement \d',
                '.*Printed in [Tt]he Netherlands'])
            params['garloc'] = dict( vtop = (3150.0, None), vbottom = (None, 0.0) )
            params['seploc'] = dict( hleft = (None, 312.7), hright = (1220.0, 1380.0),
                                    vtop = (None, None), vbottom = (None, None) )
            params['separator_re'] = '|'.join([
                '[\|1]?\d{3}[OP0]? ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP] ?([\|]|[\[I])? ?$',
                '[\|1]?\d{2}[OP]? ?([\|\[I]) ?$'])
            params['beparator_re'] = '|'.join([
                '[\|1]? ?\d{2,3} ?[OP0]? ?([\|]|[\[I] ) ?',
                '[\[\]\|I] ?\d{3}[OP0]? ?([\|]|[\[I] )? ?',
                '\d{3}[OP](?=\w)'])
            params['feparator'] = dict(
                font_re = 'GlyphLessFont',
                size = 15.49,
                s_re = '|'.join([
                    '[\|1]? ?\d{1,3} ?([OPDNI]{1,2})?([\|\[I])? ?$',
                ]),
                b_re = '|'.join([
                   '[\|1]? ?\d{3}( ?[OPDNI0]{0,2})? ?[\|\[I1] ?(?=[A-Z])',
                   '([\|]? ?|1 )?\d{2,3} ?[OPDNI0]{0,2} ?([\|\[I])? ?(?=[A-Z])',
                ]) )
            params['exceptions'] = dict(
                remove_twocolumn_titles = 9.5,
                fix_early_content = None,
                slice_markers = {
                    '2004/xml/iii102.full.json':
[('A phase I study of a 6-maleimidocaproyl-hydr:', '400P'),
('Proteolysis and angiogenesis in cancer of unknown', '411P')],
                    '2004/xml/iii15.full.json':
[('Original experimental model of study regarding the', '65P'),
('Systemic effects of surgery: Quantitative analysis of', '66P'),
('Effect of Epo on endoglin serum levels in', '67P'),
('Serum levels of EGFR, EGF and TGF am in healthy', '68P'),
('Circadian rhythm of dihydropyrimidyne','69P'),
('Prognostic significance of EGFR expression in','70P'),
('Boundaries affecting research on human biological','71P'),
('Retinoid receptors in ovarian cancer','72P'),
('Argyrophilic nucleolar organizer regions in the normal,','73P'),
('(CA)rl microsalellite polymorphism of erbB-1: a new','76P'),
('Immunohistochemical study of P70 86 kinase protein','77P'),
('Expression of estrogen receptor beta in invasive','78P'),
('Prognostic significance of ErbB-4 expression in','79P'),
('Antiapoptotic effects of rituximab combined with','80P'),
('The effect of apoptotic activity, survivin, Ki-67 and','81P'),
('TNF-alpha induced more changes in LDH isotype','82P'),
('STAT1 phosphorylation induced by interferons in','84P'),
('Invasion-metastasis by hepatocyte growth factor/c-Met','85P'),
('Serum levels of matrix metalloproteinases and their','86P'),
('Elevated expression of matrix metalloproteinaznaz:','87P'),
('Bisphosphonates inhibit adhesion to mineralised bone','88P'),
('Serum levels of matrix metalloprotinase 2 and 9 in','89P'),
('Inhibition of angiogenesis on chicken embryo','90P'),
('VEGF-C expression in breast cancer','91P'),
('Hypoxia-selective expression of systemically-delivered','93P'),
('Subcellular localization of the bovine leukemia virus','94P'),
('Evaluation of effects of concurrent S-1 (a new oral','97P'),
('Epidermal growth factor receptor TK inhibitor AG1478','98P'),
('Reverse transcriptase inhibitors down-regulate tumor','99P'),],
                    '2004/xml/iii151.full.json':
[('ldiotype specific cellular immune response in','5720'),
('Age and hemoglobin level emerge as the most','611P'),
('Ten years follow up of aggressive non-Hodgkin’s','622P'),
('The results of treatment Hodgkin’s lymphoma in','623P'),
('Dexamethason (V.A.D) for home care by the university','624P'),],
                     '2004/xml/iii217.full.json':
[('Prognostic value of 18-F-fluorodeoxyglucose (FDG)','825P'),
('Palmar-plantar erythrodysesthesia due to capecitabin','871P'),
('EGFR is a possible predictor of sensitivity to','872P'),],
                     '2004/xml/iii196.full.json':
[('High-dose interferon alpha 2b as adjuvant treatment in','778P'),
('NEUROENDOCRINE TUMORS AND CLIP _','780P'),
('lmatinib/Hydroxyurea in progressive pre-treated','7820P'),
('A phase II" trial of PTK787ZK 222584 (PTKZK), a','7840P'),],
                     '2004/xml/iii145.full.json':
[('Sequential Neoadiuvant Vinorelbine-','547P'),
('Hypercalcemic complication in patients with locally','548P'),
('ldiotype specific cellular immune response in','5720P'),],
                     '2004/xml/iii165.full.json':
[('The results of post-authorization survey in oncology','711P'),
('Gemcitabine plus cisplatin in patients with stage IIIB/IV','733P'),
('Three-days low-dose Cisplatin administration in','735P'),
('Carboplatin (C) and paclitaxel (P) combination for the','736P'),
('Gemcitabine (G) as a single drug versus gemcitabine','737P'),
('The relationship between vascular endothelial growth','739P'),
('Controversy between treatment and survival of','742P'),],
                     '2004/xml/iii211.full.json':
[('Renal safety of zoledronic acid in patients with bone','811P'),
('Role of interventional radiology in patients with','818P'),
('Cord compression and embolisation - a forgotten','819P'),
('Prognostic value of 18-F-fluorodeoxyglucose (FDG)','825P'),],
                     '2004/xml/iii206.full.json':
[('A phase II" trial of PTK787ZK 222584 (PTKZK), a','7840'),
('Brain metastases in patients with musculoskeletal','797P'),
('Acute infectious respiratory toxicity of c:=r','798P'),],
                     '2004/xml/iii12.full.json':
[('ADVANCES IN HER2 PROFILING','49'),
('Radiolabeled trastuzumab biodistribution and serum','50'),
('HER2 extracellular domain (ECD) levels do not predict','51'),],
                     '2004/xml/iii69.full.json':
[('Expression of Vascular Endothelial Growth Factor,','278'),
('Pharmacokinetics (PK) of panitumumab and','311'),
('Pelvic exenteration for colorectal and anal canal','369'),
('Clinical prognostic factors of survival in patient: with','371'),],
                     '2004/xml/iii14.full.json':
[('ADVANCES IN HER2 PROFILING','49'),
('ER2 extracellular domain (ECD) levels do not predict','50'),
('Comparison of chromogenic in-situ hybridisation','51'),
('The use of ErbB activation status as prognostic','52'),]
                }
            )
        
        elif info['year'] == '2006':
            params['laparams_char_margin'] = 1.0
            params['ignore_files'] = 'xml/(ix7|ix9|ix11|ix12|ix13|ix14|ix327|ix353)\.full\.xml'
            params['sector_flexible'] = sys.maxsize
            params['garloc'] = dict( vtop = (770.0,None), vbottom = (None,75.0) )
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology \d\d .Supplement \d',
                '^doi:10'])
            params['seploc'] = dict( hleft = (None,105.0), hright = (313.0,365.0),
                                    vtop = (None,None), vbottom = (None,None) )
            params['feparator'] = \
                dict(font_re='.*AdvHelN-L',
                     size=7.01,
                     s_re='|'.join([
                         '[\|1]? ?\d{1,3} ?([OPDNI]{1,2})?([\|\[I])? ?$',
                         'LBA\d{1,2} ?$',
                     ]),
                     b_re='|'.join([
                        '[\|1]? ?\d{3}( ?[OPDNI0]{0,2})? ?[\|\[I1] ?(?=[A-Z])',
                        '([\|]? ?|1 )?\d{2,3} ?[OPDNI0]{0,2} ?([\|\[I])? ?(?=[A-Z])',
                     ]))
            params['exceptions'] = dict(
                fix_early_content = None )
        
        elif info['year'] == '2008':
            params['laparams_char_margin'] = 1.0
            params['ignore_files'] = \
'xml/(viii2|viii3|viii4|viii7|viii9|viii10|viii11|viii13|viii289|viii312)\.full\.xml'
            params['sector_flexible'] = sys.maxsize
            params['garloc'] = dict( vtop = (770.0,None), vbottom = (None,75.0) )
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology \d\d .Supplement \d',
                '^doi:10',
                '.*Published by Oxford University Press on behalf of',
                'the European Society for Medical Oncology.'])
            params['seploc'] = dict( hleft = (None,105.0), hright = (345.0,365.0),
                                    vtop = (None,None), vbottom = (None,None) )
            params['feparator'] = \
                dict(font_re='.*AdvHelN-L',
                     size=7.01,
                     s_re='|'.join([
                         '[\|1]? ?\d{1,3} ?([OPDNI]{1,2})?([\|\[I])? ?$',
                         'LBA\d{1,2} ?$',
                     ]),
                     b_re='|'.join([
                        '[\|1]? ?\d{3}( ?[OPDNI0]{0,2})? ?[\|\[I1] ?(?=[A-Z])',
                        '([\|]? ?|1 )?\d{2,3} ?[OPDNI0]{0,2} ?([\|\[I])? ?(?=[A-Z])',
                     ]))
            params['exceptions'] = dict(
                fix_early_content = None )
        
        elif info['year'] == '2010':
            params['laparams_char_margin'] = 1.0
            params['ignore_files'] = \
'xml/(viii3|viii5|viii6|viii10|viii12|viii14|viii15|viii18|viii417|viii449|viii461)\.full\.xml'
            params['sector_flexible'] = sys.maxsize
            params['garloc'] = dict( vtop = (770.0,None), vbottom = (None,75.0) )
            params['garbage_re'] = '|'.join([
                '^Anna.+f ?Oncology \d\d .Supplement \d',
                '^doi:10',
                '.*Published by Oxford University Press on behalf of',
                'the European Society for Medical Oncology.'])
            params['seploc'] = dict( hleft = (None,105.0), hright = (345.0,365.0),
                                    vtop = (None,None), vbottom = (None,None) )
            params['feparator'] = \
                dict(font_re='.*AdvHelN-L',
                     size=7.01,
                     s_re='|'.join([
                         '[\|1]? ?\d{1,3} ?([OPDNI]{1,2})?([\|\[I])? ?$',
                         'LBA\d{1,2} ?$',
                     ]),
                     b_re='|'.join([
                        '[\|1]? ?\d{3}( ?[OPDNI0]{0,2})? ?[\|\[I1] ?(?=[A-Z])',
                        '([\|]? ?|1 )?\d{2,3} ?[OPDNI0]{0,2} ?([\|\[I])? ?(?=[A-Z])',
                     ]))
            params['exceptions'] = dict(
                fix_early_content = None )
        
        return params

if __name__ == '__main__': sys.exit(main(sys.argv))
