Tool currently available:
========================
* esmo.py

Help
====
<pre>
$ ./esmo.py --help
usage: esmo.py [-h] [-l] [-s SAVE_DIR] [--get-tocs] [--get-abstracts]
               [--download] [--all-of-them] [--pdf-do-ocr] [--pdf-to-xml]
               [--xml-to-json] [--html-to-json] [--print-abstracts]
               [--changes] [--update] [--accept] [--view] [--diff] [--inspect]
               [--processes P] [-v V]
               [YYYY [YYYY ...] [YYYY [YYYY ...] ...]]

Find, download and extract ESMO abstracts and TOCs

positional arguments:
  YYYY [YYYY ...]       Run for the given list of years

optional arguments:
  -h, --help            show this help message and exit
  -l, --list            list available years and other conference info, then
                        exits
  -s SAVE_DIR, --save-dir SAVE_DIR
                        save files into directory (default: auto_esmo)
  --get-tocs            download the conferences' TOCs
  --get-abstracts       scrapes the first 17 items from each conference and
                        saves some metadata
  --download            requires --abstracts; downloads the actual files (make
                        sure you are not behind a paywall)
  --all-of-them         requires --abstracts; process all items from each
                        conference (might result in your IP being blacklisted)
  --pdf-do-ocr          OCR downloaded pdfs and embed the text (needs
                        tesseract and convert)
  --pdf-to-xml          Convert downloaded pdfs to xml
  --xml-to-json         Extract abstracts from xml into json
  --html-to-json        Extract data from html
  --print-abstracts     Print abstracts! (from json)
  --changes             Tools to assist improving parsing parameters
  --update              requires --changes; Reprocesses xml_to_json, creating
                        prints and diffs
  --accept              requires --changes; Makes current prints the new basis
                        for comparison
  --view                requires --changes; Opens the current accepted prints
  --diff                requires --changes; Opens the last saved diffs
  --inspect             requires --view or --diff; Also opens the json from
                        xml
  --processes P         use P processes when parallelizing (default:
                        os.cpu_count())
  -v V, --verbose V     verbosity level; 0 is quiet (default: 1)
</pre>
